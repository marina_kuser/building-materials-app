import React from "react";
import styled from "styled-components";

const Styles = styled.div`
  .about {
    background: #bfb5b4;
    color: #574f4d;
  }
`;

export const About = () => (
  <Styles>
    <div>
      <div className="about">
        <h3>About Us</h3>
        <h3>Customer service is our number one priority.</h3>
      </div>
      <h4>
        We have multiple yard locations and hire professional, knowledgeable,
        customer-oriented staff.
      </h4>
      <p>
        Established in 1965, Building Materials has emerged as one of the
        premier distributors of interior building materials in southeastern
        Michigan. With stores in Southfield, Clarkston, Harrison Twp. and Troy,
        Building Materials provides some of the highest quality drywall, drywall
        finishing, steel stud/steel framing materials, insulation,
        non-combustible lumber, acoustical & drywall ceiling systems, access
        doors, FRP (fiberglass reinforced plastic), and fire-stopping materials
        available. Our employees are determined to provide the best possible
        customer service, whether it’s in our store, at our yard, or on your
        jobsite.
      </p>
      <h4>Our People</h4>
      <p>
        Many of our employees have worked at Building Materials for 10 years or
        more. We have a number of employees who started their careers in
        delivery and warehouse positions and later advanced into management.
        This is a testament to the great culture and work environment that
        Building Materials provides. We really define our employees as “family”
        and truly care about the health and welfare of everyone who works for
        us.
      </p>
      <p>
        All of Building Materials’ employees pride themselves in being hard
        working, safe, and professional in their key roles. They are the
        backbone of our success as a company.
      </p>
    </div>
  </Styles>
);
