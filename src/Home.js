import React from "react";
import styled from "styled-components";

const Styles = styled.div`
  .img-container {
    border: 1px solid #574f4d;
  }
  img {
    width: 300px;
    height: 300px;
  }
  .text-container {
    margin-top: 30px;
  }
`;

export const Home = () => (
  <Styles>
    <div>
      <h3>Your source for construction products</h3>
      <h3>
        Providing drywall, acoustical ceilings, steel framing, insulation &
        tools
      </h3>
      <div class="container img-container">
        <div className="row">
          <div class="col-sm-12 col-md-6 item">
            <img class="mx-auto d-block" src={require("./assets/block.jpg")} />
          </div>
          <div class="col-sm-12 col-md-6 item">
            <img class="mx-auto d-block" src={require("./assets/cement.jpg")} />
          </div>
        </div>
        <div className="row">
          <div class="col-sm-12 col-md-6 item">
            <img class="mx-auto d-block" src={require("./assets/knauf.jpg")} />
          </div>
          <div class="col-sm-12 col-md-6 item">
            <img class="mx-auto d-block" src={require("./assets/crijep.jpg")} />
          </div>
        </div>
      </div>
      <div class="container text-container">
        <div className="row">
          <div class="col-sm-12 col-md-3 item">
            <h5>ABOUT US</h5>
            <p>
              Teaming up with the top brands in the industry, we have one simple
              pledge: to get the right products into our customers’ hands as
              quickly and safely as possible.
            </p>
          </div>
          <div class="col-sm-12 col-md-3 item">
            <h5>OUR SERVICES</h5>
            <p>
              Building Materials carries a full range of commercial and
              residential construction supplies, and our crews are fully trained
              to deliver to commercial sites.
            </p>
          </div>
          <div class="col-sm-12 col-md-3 item">
            <h5>OUR LOCATIONS</h5>
            <p>
              Our delivery fleet is dispatched from four locations in Michigan
              (Southfield, Clarkston, Harrison Township and Troy).
            </p>
          </div>
          <div class="col-sm-12 col-md-3 item">
            <h5>CONTACT US</h5>
            <p>
              For more information about how to contact us, visit our Find a
              Yard page. Or, click the Contact Us tab to send us an email.
            </p>
          </div>
        </div>
      </div>
    </div>
  </Styles>
);
