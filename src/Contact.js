import React from "react";
import styled from "styled-components";

const Styles = styled.div`
  .row {
    background: #f4f4f4;
  }
  .item {
    border: 1px solid #574f4d;
    padding: 15px;
  }
`;

export const Contact = () => (
  <Styles>
    <div>
      <h2>Contact Us</h2>
      <h2>We look forward to hearing from you.</h2>
      <div class="container">
        <div className="row">
          <div class="col-sm-12 col-md-6 item">
            <h3>Southfield, MI</h3>
            <p>24300 Telegraph Road</p>
            <p>Southfield, Michigan 48033</p>
            <p>Phone: 248-353-2805</p>
            <p>Fax: 248-353-3175</p>
            <p>Hours: M-F 6:30 am – 4:30 pm</p>
          </div>
          <div class="col-sm-12 col-md-6 item">
            <h3>Harrison Township, MI</h3>
            <p>41841 Irwin Drive</p>
            <p>Harrison Township, Michigan 48045</p>
            <p>Phone: 586-421-8421</p>
            <p>Fax: 586-421-8424</p>
            <p>Hours: M-F 7:00 am – 4:30 pm</p>
          </div>
        </div>
        <div className="row">
          <div class="col-sm-12 col-md-6 item">
            <h3>Clarkston, MI</h3>
            <p>9700 Dixie Highway</p>
            <p>Clarkston, Michigan 48348</p>
            <p>Phone: 248-625-8995</p>
            <p>Fax: 248-625-0418</p>
            <p>Hours: M-F 7:00 am – 4:30 pm</p>
          </div>
          <div class="col-sm-12 col-md-6 item">
            <h3>Troy, MI</h3>
            <p>651 Robbins Dr</p>
            <p>Troy, MI 48083</p>
            <p>Phone: 248-250-7775</p>
            <p>Hours: M-F 6:30 am – 4:30 pm</p>
          </div>
        </div>
      </div>
    </div>
  </Styles>
);
